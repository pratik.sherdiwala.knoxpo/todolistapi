package com.example.todolist.di

import android.app.Application
import com.example.todolist.App
import com.example.todolist.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        TaskListModule::class,
        ContextModule::class,
        NetworkModule::class,
        MainActivityModule::class,
        TaskDetailModule::class,
        DatabaseModule::class
    ]
)

interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Application): Builder

        fun create(): AppComponent
    }

    fun inject(app: App)

}