package com.example.todolist.di.module

import com.example.todolist.ui.taskdetail.TaskDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TaskDetailModule {

    @ContributesAndroidInjector
    abstract fun contributeTaskDetailActivity(): TaskDetailActivity




}