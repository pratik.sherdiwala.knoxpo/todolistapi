package com.example.todolist.di.module

import com.example.todolist.ui.tasklist.TaskListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TaskListModule {

    @ContributesAndroidInjector
    abstract fun contributeTaskListActivity():TaskListActivity

}