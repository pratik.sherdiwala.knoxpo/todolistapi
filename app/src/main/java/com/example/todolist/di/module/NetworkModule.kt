package com.example.todolist.di.module

import android.content.SharedPreferences
import android.util.Log
import com.example.todolist.data.network.TaskApi
import com.example.todolist.data.repository.TaskRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    fun provideBaseUrl(): String {
        return "http://192.168.2.136:3000/"
    }

    @Provides
    fun provideHttpClient(sharedPreferences: SharedPreferences): OkHttpClient {

        val pattern =
            ("(http|https)://.*/login").toRegex()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor {
                val url = it.request().url().toString()
                Log.d("URL", url)
                val request = if (!pattern.matches(url)) {
                    it.request().newBuilder()
                        .addHeader("Authorization", sharedPreferences.getString(TaskRepository.PREF_TOKEN, null)!!)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .build()
                } else {
                    it.request().newBuilder().build()
                }
                it.proceed(request)
            }
            .build()
    }

    /*
    val httpClient = OkHttpClient.Builder()
       httpClient.addInterceptor(interceptor)
       httpClient.addInterceptor { chain ->
           val request = chain.request().newBuilder()
                   .addHeader("Authorization", BuildConfig.SPARKPOST_API)
                   .addHeader("Content-Type", "application/json")
                   .addHeader("Accept", "application/json")
                   .build()
           chain.proceed(request)
       }
       return Retrofit.Builder()
               .baseUrl(BASE_URL)
               .client(httpClient.build())
               .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
               .build()
    */

    @Provides
    @Reusable
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .create()
    }


    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideTaskApi(retrofit: Retrofit): TaskApi {
        return retrofit.create(TaskApi::class.java)
    }
}