package com.example.todolist.data.network

import com.example.todolist.model.*
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface TaskApi {

    companion object{
        const val LOGIN_PATH = "login"
    }


    @POST(LOGIN_PATH)
    fun doLogin(@Body login: Login)
            : Single<LoginResponse>


    @GET("todo")
    fun getTaskList(): Single<Todo>


    @PUT("todo/{id}")
    fun updateItem(
        @Path("id") id: Int,
        @Body taskRequest: TaskRequest
    ): Single<Task>


    @POST("todo")
    fun addTodo(
        @Body taskRequest: TaskRequest
    ): Single<TaskResonse>

    @DELETE("todo/{id}")
    fun deleteTask(
        @Path("id") id: Int
    ): Single<TaskDeleteResponse>

}