package com.example.todolist.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.todolist.data.db.converters.Converters
import com.example.todolist.data.db.dao.TaskDao
import com.example.todolist.model.Task

@Database(
    entities = [
        Task::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract val taskDao: TaskDao
}