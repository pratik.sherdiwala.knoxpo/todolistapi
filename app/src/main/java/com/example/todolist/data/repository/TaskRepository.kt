package com.example.todolist.data.repository

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.todolist.data.db.dao.TaskDao
import com.example.todolist.data.local.TaskLab
import com.example.todolist.data.network.TaskApi
import com.example.todolist.model.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class TaskRepository @Inject constructor(
    private val taskApi: TaskApi,
    private val sharedPreferences: SharedPreferences,
    private val taskDao: TaskDao
) {

    private var onGoingNetworkCall: Disposable? = null

    companion object {
        private val TAG = TaskRepository::class.java.simpleName
        val PREF_TOKEN = "$TAG.PREF_TOKEN"
    }


    fun doLogin(login: Login): Single<LoginResponse> {
        return taskApi.doLogin(login)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess {
                sharedPreferences.edit().putString(PREF_TOKEN, "Bearer " + it.token).apply()
            }
    }

    private fun getTaskFromNetwork() {
        onGoingNetworkCall?.let {
            if (!it.isDisposed) {
                return
            }
        }
        onGoingNetworkCall = taskApi.getTaskList()
            .zipWith(
                Single.defer {
                    taskDao.getAllTask().first(listOf())
                },
                BiFunction<Todo, List<Task>, List<Task>> { remoteList, localList ->
                    val mutableLocalList = localList.toMutableList()

                    remoteList.taskList.forEach { remote ->
                        val local = mutableLocalList.find { it == remote }

                        if (local != null) {
                            mutableLocalList.remove(local)
                        }
                    }
                    taskDao.insert(*remoteList.taskList.toTypedArray())
                    taskDao.deleteTask(*mutableLocalList.toTypedArray())
                    return@BiFunction remoteList.taskList
                }
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {

                },
                {
                    Log.e(TAG, "Error While Fetching", it)
                }
            )
    }

    fun updateTaskItem(id: Int, taskRequest: TaskRequest): Single<Task> {
        return taskApi.updateItem(id, taskRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun isLoggedIn(): Boolean {
        return sharedPreferences.getString(PREF_TOKEN, null) != null
    }

    fun addTask(taskRequest: TaskRequest): Single<TaskResonse> {
        return taskApi.addTodo(taskRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun deleteTask(id: Int): Single<TaskDeleteResponse> {
        return taskApi.deleteTask(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTasks(shouldRefresh: Boolean = false): Observable<List<Task>> {
        if (shouldRefresh) {
            getTaskFromNetwork()
        }
        return taskDao.getAllTask()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterNext {
                if (it.isNullOrEmpty()) {
                    getTaskFromNetwork()
                }
            }
    }
}