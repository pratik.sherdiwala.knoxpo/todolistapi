package com.example.todolist.data.db.dao

import androidx.room.*
import com.example.todolist.model.Task
import com.example.todolist.model.TaskRequest
import io.reactivex.Completable
import io.reactivex.Observable
import java.util.*

@Dao
interface TaskDao {

    @Query("SELECT * FROM TASK")
    fun getAllTask(): Observable<List<Task>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg task: Task)

    @Delete
    fun deleteTask(vararg task: Task)

}