package com.example.todolist

import android.app.Activity
import android.app.Application
import com.example.todolist.di.AppComponent
import com.example.todolist.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>


    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent
            .builder()
            .context(this)
            .create()
            .inject(this)

    }

    override fun activityInjector() = dispatchingAndroidInjector

}