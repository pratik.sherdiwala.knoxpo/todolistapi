package com.example.todolist.model

data class TaskDeleteResponse(

    val message: String
)