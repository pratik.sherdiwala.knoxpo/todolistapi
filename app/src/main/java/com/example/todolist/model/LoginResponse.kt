package com.example.todolist.model

data class LoginResponse(
    val token: String
)