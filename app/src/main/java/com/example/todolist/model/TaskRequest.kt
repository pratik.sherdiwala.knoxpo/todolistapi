package com.example.todolist.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "taskRequest")
data class TaskRequest(
    @PrimaryKey(autoGenerate = false)
    var title: String,
    var isCompleted: Boolean
)