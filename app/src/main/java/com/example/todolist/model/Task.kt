package com.example.todolist.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "task")
data class Task(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @SerializedName("title")
    val name: String,
    var isSelected: Boolean,
    var isCompleted: Int,
    var date: Date
)