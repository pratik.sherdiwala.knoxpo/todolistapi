package com.example.todolist.model

import com.google.gson.annotations.SerializedName

data class Todo(
    @SerializedName("todo")
    val taskList: List<Task>
)