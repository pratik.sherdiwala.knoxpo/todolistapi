package com.example.todolist.ui.tasklist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todolist.Event
import com.example.todolist.data.repository.TaskRepository
import com.example.todolist.model.Task
import com.example.todolist.model.TaskRequest
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private val TAG = TaskListViewModel::class.java.simpleName

class TaskListViewModel @Inject constructor(
    private val taskRepository: TaskRepository
) : ViewModel() {

    val taskList = MutableLiveData<List<Task>>()

    val isVisible = MutableLiveData<Boolean>()

    val disposable = CompositeDisposable()

    fun deleteTask(id: Int) {
        taskRepository.deleteTask(id)
            .subscribe(
                {
                    fetchTaskList()
                },
                {

                }
            ).also {
                disposable.add(it)
            }
    }

    fun fetchTaskList() {
        taskRepository.getTasks(true).subscribe(
            {
                taskList.value=it
                isVisible.value = taskList.value.isNullOrEmpty()
                Log.d(TAG, "TaskList : $it")
            },
            {
                Log.e(TAG, "Error while fetching taskList ", it)
            }
        ).also {
            disposable.add(it)
        }

    }


    private val _showDialog = MutableLiveData<Event<Task>>()
    val showDialog: LiveData<Event<Task>>
        get() = _showDialog

    fun showDialog(task: Task) {
        _showDialog.value = Event(task)
    }

    fun updateItem(id: Int, taskRequest: TaskRequest) {

        taskRepository.updateTaskItem(id, taskRequest).subscribe(
            {
                Log.d(TAG, "${it.name}")
                fetchTaskList()
            },
            {
                Log.e(TAG, "While Updating Task", it)
            }
        ).also {
            disposable.add(it)
        }
    }
}