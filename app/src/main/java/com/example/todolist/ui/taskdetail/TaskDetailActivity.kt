package com.example.todolist.ui.taskdetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.todolist.R
import com.example.todolist.databinding.ActivityTaskDetailBinding
import com.example.todolist.ui._common.DataBindingActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class TaskDetailActivity : DataBindingActivity<ActivityTaskDetailBinding>() {

    companion object {

        private val TAG = TaskDetailActivity::class.java.simpleName

        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"

        private const val ACTION_ADD = 0

        internal fun intentForAddTask(context: Context): Intent {
            return Intent(
                context,
                TaskDetailActivity::class.java
            ).apply {
                putExtra(EXTRA_ACTION, ACTION_ADD)
            }
        }
    }

    override val layoutId = R.layout.activity_task_detail

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(TaskDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {
            taskAddedEvent.observe(activity, Observer {
                it.getContentIfNotHandled()?.let {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            })

        }

        hasBack = true

//        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
//        mToolbar.setTitle(getString(R.string.app_name))
//        mToolbar.setNavigationIcon(com.example.todolist.R.drawable.ic_arrow_back_white_24dp)

//        when (intent.getIntExtra(
//            EXTRA_ACTION,
//            -1
//        )) {
//            ACTION_ADD -> {
//                addTaskBTN.setOnClickListener {
//                    viewModel.addTask()
//                }
//            }
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(com.example.todolist.R.menu.menu_task_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {


        when (item?.itemId) {
            R.id.addTask -> {
                viewModel.addTask()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }
}