package com.example.todolist.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.todolist.R
import com.example.todolist.databinding.ActivityMainBinding
import com.example.todolist.ui._common.DataBindingActivity
import com.example.todolist.ui.tasklist.TaskListActivity
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DataBindingActivity<ActivityMainBinding>(), Navigation {


    private var PRIVATE_MODE = 0


    override val layoutId = R.layout.activity_main

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {

            navigateTaskListEvent.observe(activity, Observer {

                it?.getContentIfNotHandled()?.let {
                    navigateToTaskList()
                }

            })

            successEvent.observe(activity,
                Observer {
                    it.getContentIfNotHandled()?.let {
                        navigateToTaskList()
                    }
                })

            errorEvent.observe(activity,
                Observer {
                    it.getContentIfNotHandled()?.let {
                        Snackbar.make(loginBTN, "Login Failed", Snackbar.LENGTH_SHORT).show()
                    }
                })

            checkLogin()
        }
    }


    // navigation methods
    override fun navigateToTaskList() {
        val intent = Intent(this, TaskListActivity::class.java)
        startActivity(intent)
        finish()
    }

}