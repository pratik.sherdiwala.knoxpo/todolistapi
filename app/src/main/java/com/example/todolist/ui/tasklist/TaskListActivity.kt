package com.example.todolist.ui.tasklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.R
import com.example.todolist.databinding.ActivityTaskBinding
import com.example.todolist.model.Task
import com.example.todolist.model.TaskRequest
import com.example.todolist.ui._common.DataBindingActivity
import com.example.todolist.ui.taskdetail.TaskDetailActivity
import com.example.todolist.ui.tasklist.adapter.SimpleItemTouchHelperCallback
import com.example.todolist.ui.tasklist.adapter.TaskAdapter
import com.example.todolist.ui.tasklist.adapter.viewholder.TaskNavigation
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_task.*
import javax.inject.Inject


private val TAG = TaskListActivity::class.java.simpleName

private const val REQUEST_ADD = 0

class TaskListActivity : DataBindingActivity<ActivityTaskBinding>(),
    HasSupportFragmentInjector,
    AddTaskDialogFragment.CallBack,
    TaskNavigation {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override val layoutId = R.layout.activity_task

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(TaskListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity

            viewModel = activity.viewModel

            val adapter = TaskAdapter(activity.viewModel)
            taskRV.layoutManager = LinearLayoutManager(activity)
            taskRV.adapter = adapter

            swipeRefreshLayout.setOnRefreshListener {
                swipeRefreshLayout.isRefreshing = false
                viewModel!!.fetchTaskList()
            }

            val callback = SimpleItemTouchHelperCallback(adapter)
            val itemTouchHelper = ItemTouchHelper(callback)
            itemTouchHelper.attachToRecyclerView(taskRV)
        }

        with(viewModel) {
            showDialog.observe(activity,
                Observer {
                    it.getContentIfNotHandled()?.let {
                        openDialog(it)
                    }
                })
            fetchTaskList()
        }
    }

    private fun openDialog(task: Task) {
        val ds = AddTaskDialogFragment.newInstance(task)
        ds.show(supportFragmentManager, "")
    }


    override fun taskname(name: String, id: Int) {
        val taskReq = TaskRequest(name, true)
        viewModel.updateItem(id, taskReq)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.addTaskBTN -> {
                openTaskDetail()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun openTaskDetail() {
        startActivityForResult(
            TaskDetailActivity.intentForAddTask(this),
            REQUEST_ADD
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ADD -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.fetchTaskList()
                }
            }
        }
    }


    /*
    private fun addTaskDialog() {
        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        builder.setTitle("Add Task")
        val dialogLayout = inflater.inflate(R.layout.dialog_add_task, null)
        val editText = dialogLayout.findViewById<EditText>(R.id.taskNameET)
        builder.setView(dialogLayout)
        builder.setPositiveButton("OK") { dialog, i ->
            viewModel.addTask(editText.text.toString())
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel") { dialog, i ->
            dialog.dismiss()
        }
        builder.show()
    }
    */

    /*
   private fun showDialog(id:Int) {
       val ds = AddTaskDialogFragment()
       ds.show(supportFragmentManager, "")
   }
    */
}