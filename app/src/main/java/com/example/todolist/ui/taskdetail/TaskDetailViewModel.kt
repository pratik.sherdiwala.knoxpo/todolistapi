package com.example.todolist.ui.taskdetail

import android.util.EventLog
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todolist.Event
import com.example.todolist.data.repository.TaskRepository
import com.example.todolist.model.Task
import com.example.todolist.model.TaskRequest
import com.example.todolist.model.TaskResonse
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private val TAG = TaskDetailViewModel::class.java.simpleName


class TaskDetailViewModel @Inject constructor(
    private val taskRepository: TaskRepository
) : ViewModel() {

    val taskName = MutableLiveData<String>()
    val isChecked = MutableLiveData<Boolean>()

    private val _taskAddedEvent = MutableLiveData<Event<Unit>>()

    val taskAddedEvent: LiveData<Event<Unit>>
        get() = _taskAddedEvent

    private val disposable = CompositeDisposable()

    fun addTask() {
        val taskReq = TaskRequest(
            title = taskName.value!!,
            isCompleted = isChecked.value ?: false
        )
        taskRepository.addTask(taskReq)
            .subscribe(
                {
                    _taskAddedEvent.value = Event(Unit)
                    Log.d(TAG, "Adding SuccessFully")
                },
                {
                    Log.e(TAG, "While Adding Task", it)
                }
            ).also {
                disposable.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}