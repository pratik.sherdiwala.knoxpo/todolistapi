package com.example.todolist.ui.tasklist.adapter.viewholder

interface TaskNavigation {

    fun openTaskDetail()

}