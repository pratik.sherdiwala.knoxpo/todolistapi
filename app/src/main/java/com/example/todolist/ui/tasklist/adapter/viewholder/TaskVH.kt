package com.example.todolist.ui.tasklist.adapter.viewholder

import android.content.Context
import android.os.Build
import android.view.View
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.model.Task
import kotlinx.android.synthetic.main.item_task_list.view.*
import java.text.SimpleDateFormat
import java.util.*

class TaskVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.taskTV)
    private val mtaskCB = itemView.findViewById<CheckBox>(R.id.taskCB)
    private val mTaskdateTV = itemView.findViewById<TextView>(R.id.taskDateTV)

    fun bindTask(
        task: Task,
        itemClicked: (task: Task) -> Unit,
        itemChecked: (task: Task) -> Unit
    ) {

        task.isSelected = task.isCompleted != 0

        mNameTV.text = task.name

        mTaskdateTV.text = updateDate(task.date)

        mtaskCB.setOnCheckedChangeListener(null)

        mtaskCB.isChecked = task.isSelected
        updateBackground(task.isSelected)


        itemView.setOnClickListener {
            itemClicked(task)
        }

        mtaskCB.setOnCheckedChangeListener { buttonView, isChecked ->
            task.isSelected = isChecked
            itemChecked(task)
            updateBackground(isChecked)
        }


    }

    private fun updateBackground(isSelected: Boolean) {
        itemView.setBackgroundResource(
            if (isSelected) R.color.taskItem else R.color.white
        )
    }

    private fun updateDate(date: Date): String {
        val format = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        return format.format(date)
    }
}