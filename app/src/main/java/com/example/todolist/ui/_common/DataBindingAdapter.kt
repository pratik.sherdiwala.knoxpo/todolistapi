package com.example.todolist.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.model.Task
import com.example.todolist.ui.tasklist.adapter.TaskAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("tasks")
    fun setTask(view: RecyclerView, task: List<Task>?) {
        task?.let {
            (view.adapter as? TaskAdapter)?.updateTask(it)
        }
    }
}