package com.example.todolist.ui.tasklist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.model.Task
import com.example.todolist.model.TaskRequest
import com.example.todolist.ui.tasklist.TaskListViewModel
import com.example.todolist.ui.tasklist.adapter.viewholder.TaskVH

class TaskAdapter(
    private val taskListViewModel: TaskListViewModel
) : RecyclerView.Adapter<TaskVH>(),
    ItemTouchHelperAdapter {

    private var taskList: List<Task>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskVH {

        return TaskVH(
            LayoutInflater.from(
                parent.context
            )
                .inflate(
                    R.layout.item_task_list,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = taskList?.size ?: 0

    override fun onBindViewHolder(holder: TaskVH, position: Int) {
        holder.bindTask(taskList!![position],
            {
                taskListViewModel.showDialog(it)
            },
            {
                taskListViewModel.updateItem(it.id, TaskRequest(it.name, it.isSelected))
            }
        )
    }

    fun updateTask(newTask: List<Task>) {
        taskList = newTask
        notifyDataSetChanged()
    }

    override fun onItemDismiss(position: Int) {

        val swipedItem = taskList?.get(position)

        taskListViewModel.deleteTask(
            swipedItem!!.id
        )
        // notifyItemRemoved(position)
    }
}