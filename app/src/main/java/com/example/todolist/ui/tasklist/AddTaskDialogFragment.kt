package com.example.todolist.ui.tasklist

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.example.todolist.R
import com.example.todolist.model.Task
import com.example.todolist.ui.main.MainActivity
import kotlinx.android.synthetic.main.dialog_add_task.*
import com.example.todolist.ui.tasklist.AddTaskDialogFragment.CallBack as CallBack

class AddTaskDialogFragment : DialogFragment() {

    var taskId: Int = 0

    interface CallBack {

        fun taskname(name: String, id: Int)

    }

    var callBack: CallBack? = null

    companion object {

        private val TAG = AddTaskDialogFragment::class.java.simpleName

        private val ARGS_NAME = "$TAG.ARGS_NAME"
        private val ARGS_ID = "$TAG.ARGS_ID"

        fun newInstance(task: Task) = AddTaskDialogFragment().apply {
            taskId = task.id
            arguments = Bundle().apply {
                putInt(ARGS_ID, task.id)
                putString(ARGS_NAME, task.name)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = LayoutInflater.from(context).inflate(R.layout.dialog_add_task, null, false)
        val editText = v.findViewById<EditText>(R.id.taskNameET)

        editText.setText(
            arguments?.getString(
                ARGS_NAME
            )
        )

        return AlertDialog.Builder(context)
            .setView(v)
            .setTitle("Change Task Name")
            .setPositiveButton("OK") { dialog, which ->
                val name = editText.text.toString()
                callBack?.taskname(name, taskId)
                Toast.makeText(activity, "$name", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            .create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callBack = activity as CallBack
    }

    override fun onDetach() {
        super.onDetach()
        callBack = null
    }
}