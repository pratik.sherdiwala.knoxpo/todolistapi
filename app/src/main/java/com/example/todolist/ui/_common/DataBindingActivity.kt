package com.example.todolist.ui._common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.todolist.R
import kotlinx.android.synthetic.main.include_toolbar.view.*

abstract class DataBindingActivity<T : ViewDataBinding> : ToolBarActivity() {

    abstract val layoutId: Int

    protected lateinit var binding: T
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.lifecycleOwner = this

        findViewById<Toolbar>(R.id.toolbar)?.also {
            setSupportActionBar(it)
        }
    }
}