package com.example.todolist.ui.tasklist.adapter

interface ItemTouchHelperAdapter {

    fun onItemDismiss(position: Int)

}