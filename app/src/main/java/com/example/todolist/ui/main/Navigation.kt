package com.example.todolist.ui.main

interface Navigation {

    fun navigateToTaskList();

}