package com.example.todolist.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todolist.Event
import com.example.todolist.data.repository.TaskRepository
import com.example.todolist.model.Login
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private val TAG = MainViewModel::class.java.simpleName

class MainViewModel @Inject constructor(private val taskRepository: TaskRepository) : ViewModel() {

    var username = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    private val _successEvent = MutableLiveData<Event<String>>()

    val successEvent: LiveData<Event<String>>
        get() = _successEvent

    private val disposable = CompositeDisposable()

    private val _errorEvent = MutableLiveData<Event<Unit>>()
    val errorEvent: LiveData<Event<Unit>>
        get() = _errorEvent

    private val _navigateTaskListEvent = MutableLiveData<Event<Unit>>()
    val navigateTaskListEvent: LiveData<Event<Unit>>
        get() = _navigateTaskListEvent


    fun actionOnClick() {

        taskRepository.doLogin(Login(username.value!!, password.value!!)).subscribe(
            {
                _successEvent.value = Event(it.token)
            },
            {
                Log.e(TAG, "Error while Login", it)
                _errorEvent.value = Event(Unit)
            }
        ).also {
            disposable.add(it)
        }
    }

    override fun onCleared() {
        disposable.dispose()
    }

    fun checkLogin() {
        if (taskRepository.isLoggedIn()) {
            _navigateTaskListEvent.value = Event(Unit)
        }
    }
}
