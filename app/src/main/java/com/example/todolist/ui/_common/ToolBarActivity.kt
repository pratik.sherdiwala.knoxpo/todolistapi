package com.example.todolist.ui._common

import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlin.properties.Delegates

abstract class ToolBarActivity : AppCompatActivity() {

    protected var hasBack: Boolean by Delegates.observable(false) { _, _, newValue ->
        if (newValue) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}